import os, sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

import MyComponents.MyWidget_exo2 as exo2


class MyImageViewerWidget(QFrame):

    def __init__(self, *args):

        super(MyImageViewerWidget, self).__init__(*args)
        self.setGeometry(0, 0, 800, 600)
        self.ui= exo2.Ui_Form()
        self.ui.setupUi(self)
        self.i = 0
        self.listeImage = []
        self.timer = QTimer(self)
        self.boolTime = False
        
 
    def LoadFiles(self):
        print("Loading files...")
        self.path = str(QFileDialog.getExistingDirectory(self, "Select Directory","*.jpg *png")) #Ouvre un repertoire
        if self.path:
            self.listeImage = os.listdir(self.path) #On recupere les fichiers dans une liste
            self.ui.mLineEdit.setText(str(self.path)) #On accede à mLineEdite grace à  l'import
            self.AfficherImage()

    def Next(self):
        print("Next image")
        if(self.i < len(self.listeImage)-1):
            self.i += 1
        else:
            self.i = 0
        self.AfficherImage()

    def Previous(self):
        print("Previous image")
        if(self.i > 0):
            self.i -= 1           
        else:
            self.i = len(self.listeImage)-1
        self.AfficherImage()
    
    def AfficherImage(self):
        self.px = QPixmap(str(self.path)+"/"+self.listeImage[self.i])#La liste contient le nom de l'image, il faut rajouter le path avant
        self.px = self.px.scaled(self.ui.mLabel.width(),self.ui.mLabel.height()) #Pour que l'image soit redimensionnée
        self.ui.mLabel.setPixmap(self.px)#Attache le label au pixmap
    
    def Animate(self):
        if(self.boolTime == False):
            self.timer.start(1500)
            self.timer.timeout.connect(self.Next)
            self.ui.mButtonAuto.setText("STOP")
            self.boolTime = True
        else:
            self.timer.stop()
            self.ui.mButtonAuto.setText("START")
            self.boolTime = False
        


        





class MyMainWindow(QMainWindow):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)

        # attributs de la fenetre principale
        self.setGeometry(100, 100, 1000, 800)
        self.setWindowTitle('Simple diaporama application')

        # donnée membre qui contiendra la frame associée à la widget crée par QtDesigner
        self.mDisplay = MyImageViewerWidget(self)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyMainWindow()
    w.show()
    app.exec_()