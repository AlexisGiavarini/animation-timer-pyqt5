import sys, time, random
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtMultimedia import QSound, QMediaPlayer, QMediaPlaylist, QMediaContent

import MyComponents.MyWidget_exo4 as exo4


#La methode play de QtMultimedia est bloquante (?) Ce qui empeche le son d'être joué en même temps que la methode spin
#Une solution est l'utilisation d'un player; s'il fait lagger l'application à la fin de la methode spin
#Il suffit de le retirer

playlist = QMediaPlaylist()
url = QUrl.fromLocalFile("sounds/wheel.wav")
playlist.addMedia(QMediaContent(url))
tourne = QMediaPlayer()
tourne.setPlaylist(playlist)

gagne = QSound("sounds/jackpot.wav")

class MySlotMachineWidget(QFrame):

    def __init__(self, *args):

        super(MySlotMachineWidget, self).__init__(*args)
        self.setGeometry(0, 0, 800, 600)
        self.ui= exo4.Ui_Form()
        self.ui.setupUi(self)
        self.i = 0
        self.listeImage = []
        self.boolTime = False
        self.bigImage = QImage("slot_machine_symbols.png")
        self.charger()
 
    def charger(self):

        print("Appuyez sur R pour jouer")
        largeur = self.bigImage.width()/3
        hauteur = self.bigImage.height()/3
        for i in range(1,4):
            for j in range(1,4):
                rect = QRect((j-1)*largeur,(i-1)*hauteur,largeur,hauteur)
                cropped = self.bigImage.copy(rect)
                self.listeImage.append(cropped)

        #Pour initialiser les images de la machine à sous
        self.px = QPixmap(self.listeImage[0])
        self.ui.mLabel.setPixmap(self.px)

        self.px2 = QPixmap(self.listeImage[0])
        self.ui.mLabel_2.setPixmap(self.px2)

        self.px3 = QPixmap(self.listeImage[0])
        self.ui.mLabel_3.setPixmap(self.px3)

    def spin(self):
        tourne.play()
        random.seed()
        a = None
        b = None
        c = None

        for i in range (0,20):
            time.sleep( (50 + 25 * i)/1000)

            c = self.listeImage[random.randint(0, len(self.listeImage)-1)]
            self.px3 = QPixmap(c)
            self.ui.mLabel_3.setPixmap(self.px3)

            if( i < 13 ):
                a = self.listeImage[random.randint(0, len(self.listeImage)-1)]
                self.px = QPixmap(a)
                self.ui.mLabel.setPixmap(self.px)
            if( i < 17 ):
                b = self.listeImage[random.randint(0, len(self.listeImage)-1)]
                self.px2 = QPixmap(b)
                self.ui.mLabel_2.setPixmap(self.px2)

            self.repaint()

        if((a == b) and (b == c)) :
            print("You win !")
            tourne.stop()
            gagne.play()
        else:
            print("You loose !")
            tourne.stop()

class MyMainWindow(QMainWindow):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)

        # attributs de la fenetre principale
        self.setGeometry(100, 100, 944, 323)
        self.setWindowTitle('Simple diaporama application')

        # donnée membre qui contiendra la frame associée à la widget crée par QtDesigner
        self.mDisplay = MySlotMachineWidget(self)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_R:
            self.mDisplay.spin()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyMainWindow()
    w.show()
    app.exec_()