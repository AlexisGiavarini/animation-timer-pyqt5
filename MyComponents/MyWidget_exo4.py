# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ComposantTP4.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(944, 323)
        Form.setAutoFillBackground(False)
        Form.setStyleSheet("background-color:rgb(60,60,60);")
        self.mLabel = QtWidgets.QLabel(Form)
        self.mLabel.setEnabled(True)
        self.mLabel.setGeometry(QtCore.QRect(10, 10, 300, 300))
        self.mLabel.setAutoFillBackground(False)
        self.mLabel.setStyleSheet("background-color:rgb(115, 210, 22);\n"
"")
        self.mLabel.setScaledContents(False)
        self.mLabel.setObjectName("mLabel")
        self.mLabel_2 = QtWidgets.QLabel(Form)
        self.mLabel_2.setEnabled(True)
        self.mLabel_2.setGeometry(QtCore.QRect(320, 10, 300, 300))
        self.mLabel_2.setAutoFillBackground(False)
        self.mLabel_2.setStyleSheet("background-color:rgb(115, 210, 22);\n"
"")
        self.mLabel_2.setScaledContents(False)
        self.mLabel_2.setObjectName("mLabel_2")
        self.mLabel_3 = QtWidgets.QLabel(Form)
        self.mLabel_3.setEnabled(True)
        self.mLabel_3.setGeometry(QtCore.QRect(630, 10, 300, 300))
        self.mLabel_3.setAutoFillBackground(False)
        self.mLabel_3.setStyleSheet("background-color:rgb(115, 210, 22);")
        self.mLabel_3.setScaledContents(False)
        self.mLabel_3.setObjectName("mLabel_3")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.mLabel.setText(_translate("Form", "<html><head/><body><p><span style=\" color:#73d216;\">TextLabel</span></p></body></html>"))
        self.mLabel_2.setText(_translate("Form", "<html><head/><body><p><span style=\" color:#73d216;\">TextLabel</span></p></body></html>"))
        self.mLabel_3.setText(_translate("Form", "<html><head/><body><p><span style=\" color:#73d216;\">TextLabel</span></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

