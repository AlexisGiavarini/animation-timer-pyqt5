# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ComposantTP4.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(855, 594)
        Form.setAutoFillBackground(False)
        self.mLabel = QtWidgets.QLabel(Form)
        self.mLabel.setEnabled(True)
        self.mLabel.setGeometry(QtCore.QRect(0, 0, 640, 480))
        self.mLabel.setAutoFillBackground(False)
        self.mLabel.setStyleSheet("background-color:rgb(115, 210, 22);")
        self.mLabel.setScaledContents(False)
        self.mLabel.setObjectName("mLabel")
        self.verticalLayoutWidget = QtWidgets.QWidget(Form)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(670, 10, 151, 91))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.mButtonN = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.mButtonN.setObjectName("mButtonN")
        self.verticalLayout.addWidget(self.mButtonN)
        self.mButtonP = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.mButtonP.setObjectName("mButtonP")
        self.verticalLayout.addWidget(self.mButtonP)
        self.horizontalLayoutWidget = QtWidgets.QWidget(Form)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(0, 490, 641, 80))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.mLineEdit = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.mLineEdit.setReadOnly(True)
        self.mLineEdit.setObjectName("mLineEdit")
        self.horizontalLayout.addWidget(self.mLineEdit)
        spacerItem = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.mButton = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.mButton.setObjectName("mButton")
        self.horizontalLayout.addWidget(self.mButton)

        self.retranslateUi(Form)
        self.mButton.clicked.connect(Form.LoadFiles)
        self.mButtonP.clicked.connect(Form.Previous)
        self.mButtonN.clicked.connect(Form.Next)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.mLabel.setText(_translate("Form", "<html><head/><body><p><span style=\" color:#73d216;\">TextLabel</span></p></body></html>"))
        self.mButtonN.setText(_translate("Form", "Next"))
        self.mButtonP.setText(_translate("Form", "Previous"))
        self.mButton.setText(_translate("Form", ".."))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

