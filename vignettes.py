import os, sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

import MyComponents.MyWidget_exo3 as exo3


class MyImageViewerWidget(QFrame):

    def __init__(self, *args):

        super(MyImageViewerWidget, self).__init__(*args)
        self.setGeometry(0, 0, 800, 600)
        self.ui= exo3.Ui_Form()
        self.ui.setupUi(self)
        self.i = 0
        self.listeImage = []
        self.timer = QTimer(self)
        self.boolTime = False
        self.bigImage = QImage("slot_machine_symbols.png")
        self.charger()        
 
    def charger(self):

        largeur = self.bigImage.width()/3
        hauteur = self.bigImage.height()/3
        for i in range(1,4):
            for j in range(1,4):
                rect = QRect((j-1)*largeur,(i-1)*hauteur,largeur,hauteur)
                cropped = self.bigImage.copy(rect)
                self.listeImage.append(cropped)

        #Pour mettre la premiere vignette (top-left)
        self.px = QPixmap(self.listeImage[0])
        self.ui.mLabel.setPixmap(self.px)


    def Next(self):
        print("Next image")
        if(self.i < len(self.listeImage)-1):
            self.i += 1
        else:
            self.i = 0
        self.px = QPixmap(self.listeImage[self.i])
        self.ui.mLabel.setPixmap(self.px)

    def Previous(self):
        print("Previous image")
        if(self.i > 0):
            self.i -= 1            
        else:
            self.i = len(self.listeImage)-1
        self.px = QPixmap(self.listeImage[self.i])
        self.ui.mLabel.setPixmap(self.px)
    
    def Animate(self):
        if(self.boolTime == False):
            self.timer.start(1500)
            self.timer.timeout.connect(self.Next)
            self.ui.mButtonAuto.setText("STOP")
            self.boolTime = True
        else:
            self.timer.stop()
            self.ui.mButtonAuto.setText("START")
            self.boolTime = False
        


        





class MyMainWindow(QMainWindow):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)

        # attributs de la fenetre principale
        self.setGeometry(100, 100, 1000, 800)
        self.setWindowTitle('Simple diaporama application')

        # donnée membre qui contiendra la frame associée à la widget crée par QtDesigner
        self.mDisplay = MyImageViewerWidget(self)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyMainWindow()
    w.show()
    app.exec_()